import * as React from 'react';
import { StyleSheet, Text, View, SectionList } from 'react-native';

import BothScreen, {BaseScreenProps} from "../components/BothScreen"
import ReservaItem from "../components/ReservaItem"

interface SectionDataProps {
	title: string,
	data?: Array<{
		itemName: string,
		fecha: string,
		image: any,
		price: string,
		status: string,
		lugar: string,
		persona: string
	}>
}

const DataSupport = {
	images: {
		caja: require("../assets/test/images/icon-pedir-o.png"),
		hamburguesa: require("../assets/test/images/icon-enviar-o.png"),
		leche: require("../assets/test/images/icon-listar-o.png")
	},
	tipos: {
		paquete: "Paquete",
		comida: "Comida",
		comestible: "Comestibles",
	},
	estados: ["Recogido", "Enviado"],
	horas: ["20 Jun 2020, 11:40 am", "20 Jun 2020, 11:34 am", "19 Jun 2020, 1:48 pm", "20 Jun 2020, 8:27 pm"],
	precios: [8.5, 6.5, 20.5, 16.5],
	lugar: ["Emili Wiliamson", "Silver Lesf Restaurant", "7-11 Grobery Mart", "YOLO Fast Foods"],
	personas: ["Geiner Grandez"]
}

const DataBase = [
	{
		title: "Envíos Pendientes",
		data: [
			{tipo: DataSupport.tipos.paquete, fecha: DataSupport.horas[0], icon: DataSupport.images.caja, direccion: DataSupport.lugar[0], persona: DataSupport.personas[0], precio: DataSupport.precios[0], estado: DataSupport.estados[0]},
			{tipo: DataSupport.tipos.comida, fecha: DataSupport.horas[1], icon: DataSupport.images.hamburguesa, direccion: DataSupport.lugar[1], persona: DataSupport.personas[0], precio: DataSupport.precios[1], estado: DataSupport.estados[0]},
		]
	},
	{
		title: "Past Deliveries",
		data: [
			{tipo: DataSupport.tipos.paquete, fecha: DataSupport.horas[0], icon: DataSupport.images.caja, direccion: DataSupport.lugar[0], persona: DataSupport.personas[0], precio: DataSupport.precios[0], estado: DataSupport.estados[1]},
			{tipo: DataSupport.tipos.comida, fecha: DataSupport.horas[1], icon: DataSupport.images.hamburguesa, direccion: DataSupport.lugar[1], persona: DataSupport.personas[0], precio: DataSupport.precios[1], estado: DataSupport.estados[1]},
		]
	}
]

const ReservasScreen:React.FunctionComponent<BaseScreenProps> = ({route, navigation, ...props}) => {
	return (
		<BothScreen bodyStyle={{backgroundColor: "#cacaca"}} {...{route, navigation}} title="Mis Pedidos" 
			body={(props) => {
				return <View>
					<SectionList
						sections={DataBase}
						keyExtractor={(item, index) => item.tipo+index+item.estado}
						renderItem={({ item }) => (
							<ReservaItem 
								{...item}
							/>
						)}
						renderSectionHeader={({ section: { title } }) => (
							<View style={{padding: 20}}>
								<Text style={{fontWeight: "600", color: "darkgrey", fontSize: 17}}>{title}</Text>
							</View>
						)}
					/>
				</View>
				
			}}
		/>
	)
}

export default ReservasScreen;

const styles = StyleSheet.create({
	container: {
		
	},
});