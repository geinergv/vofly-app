import * as React from 'react';
import {StyleSheet} from 'react-native';
import BothScreen, {BaseScreenProps} from '../components/BothScreen';
import IconTabLeftBar from '../components/LeftBar/IconTabLeftBar';
import Icon, {IconProps} from "../components/Icon"

type Props = BaseScreenProps & {
	
}

class PedidoScreen extends React.Component<Props> {
	
	recojo = () => {
		this.props.navigation.setOptions({setTitle: "Lugar de recojo"});
	}

	render() {
		return <BothScreen
			left={()=>{
				return (<>
					<IconTabLeftBar onPress={this.recojo} icon={(ipros) => <Icon {...ipros} from="mi" name="location-on" />} />
					<IconTabLeftBar onPress={()=>{}} icon={(ipros) => <Icon {...ipros} from="fa" name="send" size={ipros.size*0.85} />} />
					<IconTabLeftBar onPress={()=>{}} icon={(ipros) => <Icon {...ipros} from="fa" name="shopping-bag" size={ipros.size*0.85} />} />
					<IconTabLeftBar onPress={()=>{}} icon={(ipros) => <Icon {...ipros} from="io" name="ios-list-box" size={ipros.size*0.85} />} />
				</>)
			}}
		/>
	}
}

const styles = StyleSheet.create({
	
});

export default PedidoScreen;
