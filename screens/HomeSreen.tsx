//import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View, FlatList, ImageBackground } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {Card} from "react-native-paper"

import ListButton from "../components/ListButton"
import Colors from "../constants/Colors"
import Layout, {BaseScreenProps} from "../constants/Layout"
import Icon from "../components/Icon"

interface ButtonsProps {
	image?: any,
	title: string,
	go?: string,
	description: string,
	key: string,
}

const Data:ButtonsProps[] = [
	{title: "INICIAR PEDIDO", go: "Pedido", description: "is simply dummy text of the printing and typesetting", image: require("../assets/test/images/icon-pedir-o.png"), key: "pedido"},
	{title: "BUSCAR PRODUCTOS", description: "is simply dummy text of the printing and typesetting", image: require("../assets/test/images/icon-enviar-o.png"), key:"envio"},
	{title: "LISTAR TIENDAS", description: "is simply dummy text of the printing and typesetting", image: require("../assets/test/images/icon-listar-o.png"), key:"listado"},
]

const DataProm:ButtonsProps[] = [
	{title: "Order from us and get 20% Discounts", description: "Yellas Fast Food", key: "prom-1"},
	{title: "Order from us and get 20% Discounts", description: "Yellas Fast Food", key: "prom-2"},{title: "Order from us and get 20% Discounts", description: "Yellas Fast Food", key: "prom-3"},{title: "Order from us and get 20% Discounts", description: "Yellas Fast Food", key: "prom-4"},{title: "Order from us and get 20% Discounts", description: "Yellas Fast Food", key: "prom-5"},{title: "Order from us and get 20% Discounts", description: "Yellas Fast Food", key: "prom-6"},
]

const HomeScreen:React.FunctionComponent<BaseScreenProps> = ({navigation}) => {

	const items = Data.map(item => {
		return <ListButton
			key={item.key} 
			itemProps={{
				title: item.title,
				titleStyle: styles.titleItem,
				description: item.description,
				descriptionStyle: styles.descriptionItem,
				left: (props) => {
					return <View>
						<Image resizeMode="contain" style={styles.itemImage} source={item.image} />
					</View>
				}
			}}
			buttonProps={{
				activeOpacity: 0.8,
				style: styles.item,
				onPress:() => {if (item.go) navigation.push(item.go);}
			}}
		/>
	})

	return <ScrollView style={styles.container}>
		<View style={styles.bannerCnt}>
			<Card.Title style={{flex: 1}}
				title="Organiza y pide" titleStyle={{color: Colors.primary, fontSize: 32, lineHeight: 32, marginBottom: 10}}
				subtitle="con nosotros" subtitleStyle={{color: "#6974dc"}}
			/>
			<Image style={{opacity: 0.6, flex: 1,
				resizeMode: 'contain', minHeight: 150
			}}  source={require("../assets/test/images/home-banner2.png")} />
		</View>
		<View style={{padding: 10}}>
			{items}
		</View>
		<View style={styles.promContent}>
			<Text style={styles.promTitle}>Promociones</Text>
			<FlatList 
				data={DataProm}
				showsHorizontalScrollIndicator={false}
				horizontal={true}
				initialNumToRender={3}
				renderItem={({item}) => (
					<TouchableOpacity style={styles.promItem} activeOpacity={0.8} onPress={
						() => {if (item.go) navigation.push(item.go)}
					}>
						<ImageBackground source={require("../assets/test/images/def-bgi-prom.jpg")} style={{width: '100%', height: '100%'}}>
							<View style={{position: "absolute", left: 0, right:0,bottom:0,top:0, backgroundColor: "rgba(0,0,0,0.4)"}}>
							</View>
							<View style={{flex:1, padding: 10}}>
								<Text style={styles.titleItemProm}>{item.title}</Text>
								<Text style={styles.descriptionItemProm}><Icon from="mi" name="location-on" color="#f3b61d" size={18} /> {item.description}</Text>
							</View>
						</ImageBackground>
					</TouchableOpacity>
				)}
			/>
		</View>
	</ScrollView>
}

export default HomeScreen

const styles = StyleSheet.create({
	header: {
		backgroundColor: Colors.containerHomeBackground,
	},
	container: {
		flex: 1,
		backgroundColor: Colors.containerHomeBackground
	},
	bannerCnt: {
		flexDirection: "row",
		justifyContent: "space-between",
		maxWidth: 900,
	},

	// ITEM

	item: {
		padding: 10,
		backgroundColor: Colors.basePrimary,
		borderRadius: 10,
		marginBottom: 10,
		...Platform.select({
			ios: {
				shadowColor: Colors.primary,
				shadowOffset: { width: 0, height: -3 },
				shadowOpacity: 0.1,
				shadowRadius: 3,
			},
			android: {
				elevation: 10,
			},
			default: {
				shadowColor: Colors.primary,
				shadowOffset: { width: 0, height: -3 },
				shadowOpacity: 0.2,
				shadowRadius: 3,
			}
		}),
	},

	titleItem: {
		color: Colors.primary,
		fontWeight: "bold",
		fontSize: Layout.fontBase*1.2,
		marginBottom: 10,
	},

	descriptionItem: {
		letterSpacing: 1.4
	},

	itemImage: {
		width: 90,
		height:90,
	},

	// PROMOCIONES
	promContent: {
		padding: 10,
		backgroundColor: "#dce8f8",
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
	},
	promTitle: {
		color: Colors.primary,
		fontWeight: "bold",
		fontSize: Layout.fontBase*1.1,
		marginBottom: 10,
	},
	promItem: {
		height: 9*14,
		width: 16*12,
		borderRadius: 15,
		overflow: "hidden",
		marginEnd: 10
	},
	titleItemProm: {
		color: "#fff",
		fontWeight: "600",
		fontSize: Layout.fontBase*1.1,
		width: 16*9,
		flex: 1,
	},
	descriptionItemProm: {
		color: "#fff",
		fontWeight: "bold",
		fontSize: Layout.fontBase,
	},

	developmentModeText: {
		marginBottom: 20,
		color: 'rgba(0,0,0,0.4)',
		fontSize: 14,
		lineHeight: 19,
		textAlign: 'center',
	},
	contentContainer: {
		paddingTop: 30,
	},
	welcomeContainer: {
		alignItems: 'center',
		marginTop: 10,
		marginBottom: 20,
	},
	welcomeImage: {
		width: 100,
		height: 80,
		resizeMode: 'contain',
		marginTop: 3,
		marginLeft: -10,
	},
	getStartedContainer: {
		alignItems: 'center',
		marginHorizontal: 50,
	},
	homeScreenFilename: {
		marginVertical: 7,
	},
	codeHighlightText: {
		color: 'rgba(96,100,109, 0.8)',
	},
	codeHighlightContainer: {
		backgroundColor: 'rgba(0,0,0,0.05)',
		borderRadius: 3,
		paddingHorizontal: 4,
	},
	getStartedText: {
		fontSize: 17,
		color: 'rgba(96,100,109, 1)',
		lineHeight: 24,
		textAlign: 'center',
	},
	tabBarInfoContainer: {
		position: 'absolute',
		bottom: 0,
		left: 0,
		right: 0,
		...Platform.select({
			ios: {
				shadowColor: 'black',
				shadowOffset: { width: 0, height: -3 },
				shadowOpacity: 0.1,
				shadowRadius: 3,
			},
			android: {
				elevation: 20,
			},
		}),
		alignItems: 'center',
		backgroundColor: '#fbfbfb',
		paddingVertical: 20,
	},
	tabBarInfoText: {
		fontSize: 17,
		color: 'rgba(96,100,109, 1)',
		textAlign: 'center',
	},
	navigationFilename: {
		marginTop: 5,
	},
	helpContainer: {
		marginTop: 15,
		alignItems: 'center',
	},
	helpLink: {
		paddingVertical: 15,
	},
	helpLinkText: {
		fontSize: 14,
		color: '#2e78b7',
	},
});
