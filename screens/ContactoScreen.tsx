import * as React from "react"
import {StyleSheet, View, Text, TouchableOpacity} from "react-native"
import {ScrollView} from "react-native-gesture-handler"
import BothScreen from "../components/BothScreen"

import InputItem, {InputItemProps} from "../components/InputItem"

import Layout from "../constants/Layout"
import Colors from "../constants/Colors"
import Label from "../components/Label"

type InputItemPropsMap = InputItemProps & {key: string}

const Data:InputItemPropsMap[] = [
	{key: "name", title: "Nombre",editable:true, textContentType: "name", placeholder: "Ingresa tu nombre"},
	{key:"phone", title: "Télefono",editable:true, textContentType: "telephoneNumber", keyboardType:"phone-pad", placeholder:"Ingrese su número"},
	{key: "mensaje", title:"Mensaje",editable:true, placeholder:"Ingrese su mensaje"},
]

const ContactoScreen:React.FunctionComponent<{}> = (props) => {
	const inputs = Data.map(itm=><InputItem {...itm} styleCnt={{marginBottom: 20}} />) 
	return <BothScreen
		body={
			()=>(
				<View style={{flex: 1, paddingBottom: 50}}>
					<ScrollView style={styles.scroll}>
						<Label tipo="h2">
							Háganos saber sus comentarios, consultas o problemas relacionados con la aplicación o las características
						</Label>
						{inputs}
						<View style={{height: 20}} />
					</ScrollView>
					<TouchableOpacity style={styles.sendBtn} activeOpacity={0.7} >
						<Text style={styles.txtBtn}>Enviar</Text>
					</TouchableOpacity>
				</View>
			)
		}
	/>
}

const styles = StyleSheet.create({
	scroll: {
		paddingLeft: 15, 
		paddingRight: 10,
	},
	description: {
		marginTop: Layout.RadioSuperiorIzquierdo,
		marginBottom: Layout.RadioSuperiorIzquierdo,
		fontWeight: "600",
		fontSize: 20
	},
	sendBtn: {
		position: "absolute",
		bottom: 0,
		left: 0,
		right: 0,
		backgroundColor: Colors.primary,
		height: 50,
		justifyContent: "center",
		padding: 10,
		borderTopLeftRadius: Layout.RadioSuperiorIzquierdo,
	},
	txtBtn: {
		fontSize: 22,
		textAlign: "center",
		color: Colors.basePrimary,
	},
})

export default ContactoScreen