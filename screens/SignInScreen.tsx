import React, {Component} from "react"
import { View, StyleSheet, Image } from "react-native";
import BothScreen, {BaseScreenProps} from "../components/BothScreen";
import InputItem from "../components/InputItem";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from '@react-navigation/native';

import { connect, ConnectedProps } from 'react-redux'
import {RootState} from '../store/redux/reducers'
import {setAuth} from '../store/redux/actions';

import VerificationCodeModal from "../components/VerificationCodeModal";
import { VoFlyAuth, VerifyPhone } from "../constants/App";

import Label from "../components/Label";
import { Text } from "react-native-paper";
import Colors from "../constants/Colors";
import Layout from "../constants/Layout";

//import { VoFlyAuth } from "../constants/App";
const mapState = (state: RootState) => ({
	auth: state.app.auth,
});

const mapDispatch = {
	setAuth
}

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & BaseScreenProps & {};

interface States {
	phone: string,
	modalVisible: boolean,
	email: string,
	isLoadingSign: boolean,
	verifyData?: VerifyPhone,
	errors?: {
		email?: string[],
		phone?: string[],
	}
}

class SignInScreen extends Component<Props, States> {
	//const auth = VoFlyAuth.getCachedAuth()
	state = {
		email: "",
		phone: "",
		modalVisible: false,
		isLoadingSign: false,
		verifyData: undefined,
		errors: undefined,
	}
	constructor(props) {
		super(props);
		this.navigation = props.navigation;
		if (props.auth?.preregistered) this.navigation.navigate("SignUp");
		if (props.auth?.isRegistered && !props.auth?.isLogged) {
			const { email, phone, verify_id_data} = props.auth.user;
			this.state = {...this.state,
				email, phone, modalVisible: true,
				verifyData: verify_id_data
			};
			if (!verify_id_data.available) {
				this.submit();
			}
		}
		//autocomplete
		this.state = {...this.state, email: "Aaa@aaa.com", phone: '741258963'};
	}

	setLoading = (load:boolean) => {
		this.setState({isLoadingSign: load});
		if (load) {
			setTimeout(() => { this.setState({isLoadingSign: false}); }, 3000);
		}
	}

	_setPhone = (text:string) => {
		this.setState({phone: text});
	}
	_setEmail = (text:string) => {
		this.setState({email: text});
	}

	setModalVisible = (val:boolean) => {
		this.setState({modalVisible: val});
	}

	submit = () => {
		const {phone, email} = this.state;
		console.log("Submit prelogin ===");
		if (!this.state.isLoadingSign) {
			this.setLoading(true);
			//this.setState({isLoadingSign: true});
			this.prelogin(phone, email);
				//.finally(()=>this.setState({ isLoadingSign: false }));
		}
	}
	
	prelogin = async (phone:string, email:string) => {
		//this.setState({isLoadingSign: true});
		const verify_id = this.state.verifyData && this.state.verifyData.verify_id;
		const result = await VoFlyAuth.preloginAsync(email, phone, verify_id);
		if (result?.data) {
			this.setState({modalVisible: true, verifyData: result.data});
		} else if (result?.errors) {
			this.setState({errors: result.errors});
			if (result?.errors?.verify_id) {
				this.setState({verifyData: undefined});
			}
		}
		this.setLoading(true);
		//this.setState({ isLoadingSign: false });
		console.log(result);
	}

	submitCode = (code:string) => {
		if (!this.state.isLoadingSign, this.state?.verifyData) {
			this.setLoading(true);
			this.login(code);
		}
	}

	login = async (code:string) => {
		const verify_id = this.state?.verifyData.verify_id;
		console.log("Prev Login:", {verify_id, code});
		const result = await VoFlyAuth.loginAsync(verify_id, code);
		console.log("Result Login:", result);
		if (result?.data) {
			this.props.setAuth(await VoFlyAuth.getCachedAuth());
		} else if (result?.errors) {
			this.setState({errors: result.errors});
		}
		this.setLoading(false);
	}

	render () {
		return <BothScreen 
		    header={()=>{
		        return <View style={styles.headerLogo} >
		            <Image height={vars.heiHeader} source={require("../assets/images/logo.png")} />
		        </View>
		    }}
		    body={()=>{
		        return <>
					<View style={styles.body}>
		                <ScrollView style={styles.scroll}>
		                    <Label tipo="h4" style={{
		                        marginTop: 10, paddingLeft: 10, color: Colors.textMuted,
		                        marginBottom: 30,
		                        textAlign: "center"
		                    }}>
		                        Ingrese con su correo electrónico
		                    </Label>
		                    <InputItem styleCnt={{marginBottom: 40}} value={this.state.email} placeholder="Seleccione" title="Correo" onChangeText={this._setEmail} keyboardType="email-address" textContentType="emailAddress" editable />
		                    <InputItem title="Teléfono" value={this.state.phone} placeholder="Ingrese su número" keyboardType={"phone-pad"} textContentType="telephoneNumber" onChangeText={this._setPhone} editable />
		                    <Label tipo="p" style={{
		                        marginTop: 30, marginBottom: 10,
		                        textAlign: "center",
		                        fontWeight: "bold"
		                    }}>O</Label>
		                    <Label tipo="h2" style={{
		                        marginTop: 0, color: Colors.link,
		                        textAlign: "center"
		                    }}
		                        onPress={()=>this.navigation.navigate("SignUp")}
		                    >¿Aún no tienes una cuenta?</Label>
		                </ScrollView>
		                <TouchableOpacity style={styles.sendBtn} activeOpacity={0.7} onPress={this.submit} >
		                    <Text style={styles.txtBtn}>Enviar</Text>
		                </TouchableOpacity>
		            </View>
					<VerificationCodeModal submit={this.submitCode} transparent={true} verifyData={this.state.verifyData} modalVisible={this.state.modalVisible} close={()=>this.setState({modalVisible: false})} />
				</>
		    }}
		/>
	}
}

export default connector(SignInScreen);

const vars = {
    heiHeader: 120
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        /* paddingBottom: 50,
        backgroundColor: "red",
        borderBottomWidth: 3,
        borderBottomColor: "pink",
        position: "relative", */
    },
    headerLogo: {
        height: vars.heiHeader,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    scroll: {
		paddingLeft: 15, 
		paddingRight: 10,
    },
    sendBtn: {
        marginTop: 10,
		backgroundColor: Colors.primaryLight,
        height: 50,
		justifyContent: "center",
		padding: 10,
		borderTopLeftRadius: Layout.RadioSuperiorIzquierdo,
	},
	txtBtn: {
		fontSize: 22,
		textAlign: "center",
		color: Colors.basePrimary,
	},
})
