import * as React from "react"
import {StyleSheet, View, Text, TouchableOpacity} from "react-native"
import {ScrollView} from "react-native-gesture-handler"

import BothScreen, {BaseScreenProps} from "../components/BothScreen"

import Layout from "../constants/Layout"
import Colors from "../constants/Colors"
import Label from "../components/Label"

const ContactoScreen:React.FunctionComponent<BaseScreenProps> = ({route, navigation}) => {
	return <BothScreen
		title="Términos y Condiciones"
		route={route} navigation={navigation}
		body={
			()=>(
				<View style={{flex: 1}}>
					<ScrollView style={styles.scroll}>
						<Label tipo="h2">Política de privacidad de la empresa</Label>
						<Label tipo="p">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, quasi inventore, ipsa magnam pariatur unde velit impedit voluptatibus blanditiis repellendus dignissimos nihil omnis, quisquam repudiandae accusantium! Officiis quos ullam qui. Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe incidunt eveniet reprehenderit numquam et cum. Voluptatem quos consequatur sint velit beatae accusamus eveniet minima in, eaque voluptatibus voluptate veritatis itaque non aliquid, suscipit provident aliquam explicabo.
						</Label>
						<Label tipo="p">
							Quos ullam recusandae voluptates, quo voluptatibus labore maiores veniam iusto necessitatibus, autem dolor est sunt soluta hic qui deserunt. Placeat vitae adipisci voluptatem repudiandae velit quaerat natus ipsa ipsum ad doloribus odit deleniti veritatis, eius corrupti ipsam cupiditate aut, sint qui dolor, ut nesciunt! Delectus saepe reprehenderit fugiat voluptatem quos temporibus ut iusto iste tenetur aperiam ipsam, quo quia distinctio debitis, consectetur magnam animi.
						</Label>
						<Label tipo="p"> 
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe tempora dolorem earum omnis officia eius repellat iusto doloribus iste, quam ea excepturi deleniti ab, exercitationem itaque ut corrupti atque quasi fugit deserunt voluptas velit. Saepe similique magnam veniam, debitis eligendi exercitationem eos omnis atque. Excepturi voluptates odit fugiat commodi nihil atque rem eligendi, quaerat debitis? Accusamus ex rem sint doloremque quam quo excepturi, ad incidunt maiores amet, similique reiciendis voluptatum voluptas eum optio doloribus deleniti modi explicabo vel veniam et quibusdam aspernatur odio pariatur!
						</Label>
						<Label tipo="p">
							Magni, voluptates in illum excepturi, id vero asperiores nulla repellendus est numquam corrupti quaerat dolorum, cupiditate deleniti atque veritatis exercitationem modi libero! Necessitatibus rem sapiente doloribus eos eius vitae commodi deserunt, perferendis neque esse dolor dolorum? Deleniti facere esse odio, nulla aliquid ex error, labore eum iure, expedita corporis quibusdam? Alias debitis velit architecto magnam esse.
						</Label>
						{/* <View style={{height: 10}} /> */}
					</ScrollView>
				</View>
			)
		}
	/>
}

const styles = StyleSheet.create({
	scroll: {
		paddingLeft: 15,
		paddingRight: 10
	}
})

export default ContactoScreen