import * as React from "react"
import { View, StyleSheet} from 'react-native';
import {ScrollView} from "react-native-gesture-handler"
import {Avatar} from "react-native-paper";

import Colors from "../constants/Colors"
import Layout, {BaseScreenProps} from "../constants/Layout"
import InputItem, {InputItemProps} from "../components/InputItem"
//import Resource from "../constants/Resource"
//import { VoFlyAuth } from "../constants/App";
import { connect, ConnectedProps } from 'react-redux'
import {RootState} from '../store/redux/reducers';
//import {setAuth, signoutAuth} from '../store/redux/actions';
import { useNavigation } from '@react-navigation/native';

const mapState = (state: RootState) => ({
	auth: state.app.auth,
})

const connector = connect(mapState);

type PropsFromRedux = ConnectedProps<typeof connector>;

const Data:InputItemProps[] = [
	{title: "Nombre", textContentType: "name", value: "Geiner Grandez"},
	{title:"Correo", textContentType: "emailAddress", keyboardType: "email-address", value:"geiner120300@gmail.com"},
	{title: "Télefono", textContentType: "telephoneNumber", keyboardType:"phone-pad", value:"+19876543210"}
]

type PerfilScreenProps = PropsFromRedux & {
	dataForm: InputItemProps[]
};

const PerfilScreen:React.FunctionComponent<PerfilScreenProps> = (props) => {
	const navigation = useNavigation();
	navigation.setOptions({ headerTitle: "Mi Perfil" });
	const inputs = Data.map(val=> <InputItem {...val} key={val.title} /> )
	return (
		<View style={styles.container}>
			<View style={styles.fondo}></View>
			<ScrollView>
				<View style={{alignItems: "center"}}>
					<Avatar.Image style={styles.avatar} size={LocalLayout.avatar.size} source={require("../assets/test/images/avatar.jpg")} />	
				</View>
				<View style={styles.form}>
					{inputs}
				</View>
			</ScrollView>
		</View>
	);
}

export default connector(PerfilScreen)

const LocalLayout = {
	avatar: {
		size: 110,
		margin: 20,
		topPercent: 0.4
	}
}

const styles = StyleSheet.create({
	 container: {
		flex: 1,
		backgroundColor: Colors.containerBackground
	 },
	 fondo: {
		backgroundColor: Colors.containerMainBackground,
		borderTopLeftRadius: Layout.RadioSuperiorIzquierdo,
		position: "absolute",
		bottom: 0,
		left: 0,
		right:0,
		top: LocalLayout.avatar.margin + LocalLayout.avatar.size * LocalLayout.avatar.topPercent,
	 },
	 form: {
		paddingLeft: 15,
		paddingRight: 10,
	 },
	 avatar: {
		marginTop: LocalLayout.avatar.margin,
		marginBottom: LocalLayout.avatar.margin,
		// position: "relative",
		// top: -(LocalLayout.avatar.topPercent)*LocalLayout.avatar.size,
	 }
})
