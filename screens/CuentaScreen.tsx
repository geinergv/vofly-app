import * as React from 'react';
import {
	View, StyleSheet,
	FlatList, Text,
	TouchableOpacity
 } from 'react-native';
import {List, Avatar} from "react-native-paper";
//import {ScrollView, RectButton, RectButtonProperties} from "react-native-gesture-handler";
//import { createStackNavigator } from '@react-navigation/stack';

import {stylesLabel} from "../components/Label"
import Colors from "../constants/Colors"
import Layout from "../constants/Layout"
import BothScreen from "../components/BothScreen"
import Icon, {IconProps} from "../components/Icon"
import {ListButton} from "../components/ListButton"


import { VoFlyAuth } from "../constants/App";
import { connect, ConnectedProps } from 'react-redux'
import {RootState} from '../store/redux/reducers';
import {setAuth, signoutAuth} from '../store/redux/actions';
import { useNavigation } from '@react-navigation/native';

const mapState = (state: RootState) => ({
	auth: state.app.auth,
})

const mapDispatch = {
	setAuth,
	signoutAuth,
}

const connector = connect(mapState, mapDispatch);

//import PerfilScreen from "./Perfil";

type Props = PropsFromRedux & {};

interface dataProps {
	key: string,
	title: string,
	description: string,
	go?: string,
	icon?: IconProps
}

const DataOfList:dataProps[] = [
	{key: "cuenta-direccion", icon: {name:"location-on", from:"mi"}, title: "Guarde su dirección", description: "Guarde su dirección"},
	{key: "cuenta-contacto", icon: {name:"mail", from:"mi"}, title: "Contacto", description: "Contáctenos", go: "Contacto"},
	{key: "cuenta-condiciones", icon: {name:"clipboard-text", from:"mci"}, title: "Términos y Condiciones", description: "Lee los términos y condiciones", go:"Condiciones"},
	{key: "cuenta-comparte", icon: {name:"share", from:"fa"}, title: "Comparte la App", description: "Comparte con amigos y familia"},
	{key: "cuentaSignout", icon: {name:"sign-out", from:"fa"}, title: "Cerrar sesión", description: "Cerrar sesión de su cuenta"},
]

const signOutData = {key: "cuentaSignout", icon: {name:"sign-out", from:"fa"}, title: "Cerrar sesión", description: "Cerrar sesión de su cuenta"};

//const CuentaStack = createStackNavigator()

const CuentaScreen:React.FunctionComponent<Props> = (props) => {
	const navigation = useNavigation();
	const [selected, setSelected] = React.useState();
	const onSelect = (id:any) => {
		setSelected(id);
	  }
	
	const handleSignout = async () => {
		console.log("signOut");
		await VoFlyAuth.signoutAsync()
		props.signoutAuth();
	}
	const handler:any = {
		"cuentaSignout": handleSignout
	}
	/*const listItems = DataOfList.map((item) =>
		<ListButton
			key={item.key}
			buttonProps={{style: styles.btnItem,
				activeOpacity: 0.7,
				onPress: ()=>{
					if (item.go) navigation.push(item.go)
				}
			}}
			itemProps={{
				style: {
					paddingLeft: Layout.RadioSuperiorIzquierdo - 10,
					paddingRight: 10
				},
				title: item.title,
				titleStyle: styles.itemTitle,
				description: item.description,
				descriptionStyle: styles.itemDescription,
				left: (props)=> 
					<View {...props} style={styles.iconCnt}>
						<Icon {...item.icon} size={30} color={Colors.primary} />
					</View>
			}}
		/>
	)*/
	return (<BothScreen
	  header={
		  (props)=> (
			<ListButton
				buttonProps={{ activeOpacity:0.8, onPress: ()=>{navigation.push("Perfil")}} } 
				itemProps={{
					title: "Geiner Grandez Valle",
					description: "Ver Perfil", titleStyle: styles.perfilTitle,
					descriptionStyle: styles.perfilDescription,
					left: (props) => 
						<Avatar.Image style={styles.avatar} size={80} source={require('../assets/test/images/avatar.jpg')} /> 
				}}
			/>
		  )
		}
		headerStyle={styles.perfilBtnContent}
		body={
			(props)=>(
				<>
					<FlatList style={styles.flatList}
						data={DataOfList}
						renderItem={({ item }) => (
							<ListButton 
								buttonProps={{style: styles.btnItem,
									activeOpacity: 0.7,
									onPress: ()=>{
										if (handler[item.key]) handler[item.key]();
										else if (item.go) navigation.push(item.go);
									}
								}}
								itemProps={{
									style: {
										paddingLeft: Layout.RadioSuperiorIzquierdo - 10,
										paddingRight: 10
									},
									title: item.title,
									titleStyle: styles.itemTitle,
									description: item.description,
									descriptionStyle: styles.itemDescription,
									left: (props)=> 
										<View {...props} style={styles.iconCnt}>
											<Icon {...item.icon} size={30} color={Colors.primary} />
										</View>
								}}
							/>
						)}
					/>
				</>
			)
		}
	/>)
}

export default connector(CuentaScreen);

const styles = StyleSheet.create({
	container: {
		backgroundColor:  Colors.primary,
		flex: 1,
		color: Colors.tintColorOnPrimary
	},

	// PERFIL

	perfilBtnContent: {
		paddingRight: 15,
		paddingLeft: 15,
		marginBottom: 10,
	},

	perfilTitle: {
		color: Colors.tintColorOnPrimary,
		fontSize: 20,
		fontWeight: "bold"
	},
	perfilDescription: {
		color: Colors.tintColorOnPrimary,
		fontSize: 15
	},
	avatar: {
		marginRight: 10
	},

	// LIST

	flatList: {
		
	},
	btnItem: {

	},
	itemTitle: {
		...(stylesLabel.default)
	},
	itemDescription: {
		fontSize: 16,
		color: "#bdbdbd"
	},
	iconCnt: {
		width: 40,
		alignItems: "center",
		justifyContent: "center",
		marginRight: 16
	}
})
