import React, {Component, EventHandler, SyntheticEvent} from "react"
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { View, StyleSheet, Image, Platform } from "react-native";
import * as ImagePicker from 'expo-image-picker';
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import { Avatar, Text } from "react-native-paper";

import Icon from "../components/Icon"
import BothScreen, {BaseScreenProps} from "../components/BothScreen";
import Colors from "../constants/Colors";
import Layout from "../constants/Layout";
import InputItem from "../components/InputItem";
import AppDta, { VoFlyAuth, VerifyPhone } from "../constants/App";
import VerificationCodeModal from "../components/VerificationCodeModal";

import { connect, ConnectedProps } from 'react-redux'
import {RootState} from '../store/redux/reducers';
import {setAuth} from '../store/redux/actions';

const mapState = (state: RootState) => ({
	auth: state.app.auth,
})

const mapDispatch = {
	setAuth
}

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {};

type ImagePickerResult = {
	uri: string;
	width: number;
	height: number;
	type?: 'image' | 'video';
	exif?: { [key: string]: any };
	base64?: string;
};

interface StateProps {
	image?: string,
	name?: string,
	direccion?: string,
	phone?: string,
	modalVisible: boolean,
	email?: string,
	isLoadingSign: boolean,
	verifyData?: VerifyPhone,
	errors?: {
		email?: string[],
		name?: string[],
		phone?: string[],
		direccion?: string[],
	}
}
//= ({navigation, ...props}) =>
class SignUpScreen extends Component<Props, StateProps> {
	state = {
		image: "",
		name: "",
		direccion: "",
		email: "",
		phone: "",
		modalVisible: false,
		isLoadingSign: false,
		verifyData: undefined
	}

	constructor(props) {
		super(props);
		if (props.auth?.preregistered && !props.auth?.isLogged) {
			const {name, email, phone, direccion, verify_id_data} = props.auth.user;
			this.state = {...this.state,
				name, email, phone, direccion, modalVisible: true,
				verifyData: verify_id_data
			};
			if (!verify_id_data.available) {
				this.submit();
			}
		}
	}

	setLoading = (load:boolean) => {
		this.setState({isLoadingSign: load});
		if (load) {
			setTimeout(() => { this.setState({isLoadingSign: false}); }, 3000);
		}
	}

	_setName = (text:string) => {
		this.setState({name: text});
	}
	_setPhone = (text:string) => {
		this.setState({phone: text});
	}
	_setDireccion = (text:string) => {
		this.setState({direccion: text});
	}
	_setEmail = (text:string) => {
		this.setState({email: text});
	}

	setModalVisible = (val:boolean) => {
		this.setState({modalVisible: val});
	}

	render() {
		//const {navigation} = this.props;
		const {image} = this.state;
		if (this.props.auth?.isLogged) return null;
		return <BothScreen
			body={()=>{
				return <>
					<ScrollView style={styles.scroll}>
						<View style={styles.imgPerfilCnt}>
							<TouchableOpacity activeOpacity={.7} onPress={this._pickImage}>
								<Icon from="mi" name="insert-photo" color={Colors.primaryLight} size={40} />
							</TouchableOpacity>
							{ image ?
								<Image source={{ uri: image }} style={styles.imgPerfil} /> : 
								<Avatar.Text label="?" style={styles.imgPerfil} />
							}
							<TouchableOpacity activeOpacity={.7} disabled>
								<Icon from="mi" name="photo-camera" color={Colors.primaryLight} size={40} />
							</TouchableOpacity>
						</View>
						<InputItem title="Nombre" nativeID="registro_name_input" value={this.state.name} placeholder="Ingrese su nombre" keyboardType="default" textContentType="name" editable styleCnt={styles.inputItem} onChangeText={this._setName} />
						<InputItem title="Dirección" nativeID="registro_direccion_input" value={this.state.direccion} placeholder="Ingrese dirección" keyboardType="default" textContentType="fullStreetAddress" editable styleCnt={styles.inputItem} onChangeText={this._setDireccion} />
						<InputItem title="Correo Electrónico" nativeID="registro_email_input" value={this.state.email} placeholder="Ingrese correo" keyboardType="email-address" textContentType="emailAddress" editable styleCnt={styles.inputItem} onChangeText={this._setEmail} />
						<InputItem title="País" placeholder="Seleccione" defaultValue="Perú" keyboardType="default" disabled textContentType="countryName" editable styleCnt={styles.inputItem} />
						<InputItem title="Teléfono" maxLength={9} nativeID="registro_phone_input" value={this.state.phone} placeholder="Ingrese número" keyboardType="numeric" textContentType="telephoneNumber" editable styleCnt={styles.inputItem} onChangeText={this._setPhone} />
					</ScrollView>
					<TouchableOpacity style={styles.sendBtn} activeOpacity={0.7} onPress={this.submit} >
                        <Text style={styles.txtBtn}>Enviar</Text>
                    </TouchableOpacity>
					<VerificationCodeModal submit={this.submitCode} transparent={true} verifyData={this.state.verifyData} modalVisible={this.state.modalVisible} close={()=>this.setState({modalVisible: false})} />
				</>
			}}
		/>
	}
	componentDidMount() {
		this.getPermissionAsync();
	}
	getPermissionAsync = async () => {
		if (Constants.platform?.ios) {
			const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
				if (status !== 'granted') {
				alert('Lo sentimos, ¡necesitamos permisos de la galería para hacer que esto funcione!');
			}
		}
	};

	_pickImage = async () => {
		try {
			let result = await ImagePicker.launchImageLibraryAsync({
				mediaTypes: ImagePicker.MediaTypeOptions.Images,
				allowsEditing: true,
				aspect: [1, 1],
				quality: 1,
			});
			if (!result.cancelled) {
				this.setState({ image: result.uri});
			}

			//console.log(result);
		} catch (E) {
			console.log(E);
		}	
	};

	submit = () => {
		const {name, direccion, phone, email} = this.state
		if (!this.state.isLoadingSign) {
			this.setLoading(true);
			//this.setState({isLoadingSign: true});
			this.preregistrar(name, direccion, phone, email);
				//.finally(()=>this.setState({ isLoadingSign: false }));
		}
	}

	submitCode = (code:string) => {
		if (!this.state.isLoadingSign, this.state?.verifyData) {
			this.setLoading(true);
			this.registrar(code);
		}
	}

	registrar = async (code:string) => {
		const {email, name, direccion} = this.state;
		const verify_id = this.state?.verifyData.verify_id;
		console.log("Prev Registro:", {code, email, name, direccion, verify_id});
		const result = await VoFlyAuth.registerAsync({code, email, name, direccion, verify_id});
		console.log("Result Registro:", result);
		if (result?.data) {
			this.props.setAuth(await VoFlyAuth.getCachedAuth());
		} else if (result?.errors) {
			this.setState({errors: result.errors});
		}
		this.setLoading(false);
	}
	
	preregistrar = async (name:string, direccion:string, phone:string, email:string) => {
		//this.setState({isLoadingSign: true});
		const verify_id = this.state.verifyData && this.state.verifyData.verify_id;
		const result = await VoFlyAuth.preregisterAsync({name, direccion, phone, email,
			...(verify_id ? {verify_id} : {})
		});
		if (result?.data) {
			this.setState({modalVisible: true, verifyData: result.data});
		} else if (result?.errors) {
			this.setState({errors: result.errors});
			if (result?.errors?.verify_id) {
				this.setState({verifyData: undefined});
			}
		}
		this.setLoading(true);
		//this.setState({ isLoadingSign: false });
		console.log(result);
	}
}

const vars = {
	perfil: {
		size: 140
	}
}

const styles = StyleSheet.create({
    scroll: {
		paddingLeft: 15, 
		paddingRight: 10,
    },
	imgPerfilCnt: {
		paddingTop: 10,
		paddingBottom: 10,
		display: "flex",
		flexDirection: "row",
		flexWrap: "nowrap",
		alignItems: "center",
		justifyContent: "space-around"
	},
	imgPerfil: {
		width: vars.perfil.size,
		height: vars.perfil.size,
		borderRadius: vars.perfil.size,
		backgroundColor: Colors.textMuted,
	},
	inputItem: {
		marginBottom: 20,
	},
    sendBtn: {
        marginTop: 10,
		backgroundColor: Colors.primaryLight,
        height: 50,
		justifyContent: "center",
		padding: 10,
		borderTopLeftRadius: Layout.RadioSuperiorIzquierdo,
	},
	txtBtn: {
		fontSize: 22,
		textAlign: "center",
		color: Colors.basePrimary,
	},
})

export default connector(SignUpScreen);
