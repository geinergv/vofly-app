export const SET_AUTH = "SET_AUTH";
export const PREREGISTER_AUTH = "PREREGISTER_AUTH";
export const REGISTER_AUTH = "REGISTER_AUTH";
export const LOGIN_AUTH = "LOGIN_AUTH";
export const SIGNOUT_AUTH = "SIGNOUT_AUTH";

// states

export interface AppState {
	auth: VoFlyAuthType,
}

// actions

export interface SetAuthAction {
	type: typeof SET_AUTH,
	payload: VoFlyAuthType,
}

export interface SetAuthAction {
	type: typeof SIGNOUT_AUTH
}

export type AppActionTypes = SetAuthAction | SetAuthAction
