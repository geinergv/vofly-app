
import {SET_AUTH, SIGNOUT_AUTH, AppActionTypes} from './types';
import {VoFlyAuthType} from '../../constants/App';

export const setAuth = (auth:VoFlyAuthType): AppActionTypes => {
	return {type: SET_AUTH, payload: auth};
}

export const signoutAuth = (): AppActionTypes => {
	return {type: SIGNOUT_AUTH};
}
