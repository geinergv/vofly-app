import {combineReducers} from 'redux';
//import tabBarReducer from './tabBar.reducer'
//import dataReducer from './data.reducer'
import {VoFlyAuthType} from '../../constants/App';
import {
	SET_AUTH,
	SIGNOUT_AUTH,
	AppState
} from './types';

const initialState:AppState = {
	auth: null,
}

const appReducer = (state=initialState, action: AppActionTypes) => {
	switch(action.type) {
		case SET_AUTH:
			console.log(action);
			return {...state, auth: {...action.payload}};
		case SIGNOUT_AUTH:
			return {...state, auth: null};
		default:
			return state;
	}
}

const rootReducer = combineReducers({
    //tabId: tabBarReducer,
    //data: dataReducer
	app: appReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer;
