//export const SELECT_DESTINO = 'SELECT_DESTINO';
export const SELECT_PLACE_AGENTE = 'SELECT_PLACE_AGENTE';
export const SELECT_PLACE_SHOP = 'SELECT_PLACE_SHOP';

export const SELECT_PLACE_PEDIDO_RECOJO = 'SELECT_PLACE_PEDIDO_RECOJO';
export const SEARCH_PLACE_PEDIDO = 'SEARCH_PLACE_PEDIDO';
export const GO_PEDIDO_RECOJO_VIEW = 'GO_PEDIDO_RECOJO_VIEW';

export const DESTINO_RECOJO = 'DESTINO_RECOJO';
export const DESTINO_ENTREGA = 'DESTINO_ENTREGA';
export const DESTINO_AGENTE = 'DESTINO_AGENTE';
export const DESTINO_TIENDA = 'DESTINO_TIENDA';
export const DESTINO_FOOD = 'DESTINO_FOOD';
export const DESTINO_MERCADO = 'DESTINO_MERCADO';
export const DESTINO_PAQUETE = 'DESTINO_PAQUETE';


export type DestinoOrdenType = DESTINO_RECOJO|DESTINO_ENTREGA;
export type DestinoAgentTienda = DESTINO_AGENTE|DESTINO_TIENDA;

export interface Lugar {
	nombre?: string,
	coordenadas: {
		lat: number,
		long: number
	},
	direccion: string,
	identificador?: string,	
}

export interface Destino {
	lugar: Lugar,
	phone: string,
}

export interface Agente extends Destino {
	landmark: string,
	fullname: string
}

export interface Shop extends Destino {
	name: string
}

export type RecojoType = Agente | Shop;

export type EntregaType = Agente;




