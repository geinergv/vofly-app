import * as React from 'react';
import { Platform, StatusBar, StyleSheet, View, Text } from 'react-native';
import { SplashScreen } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import {Provider} from 'react-redux';
import configureStore from './store/redux/configure'
import {setAuth as setAuthAction} from './store/redux/actions'

import AppNavigator from './navigation/AppStackNavigator';
import SignStackNavigator from "./navigation/SignStackNavigator";
import useLinking from './navigation/useLinking';
import Colors from "./constants/Colors"
import {VoFlyAuth} from "./constants/App"
import AuthNavigation from './navigation/AuthNavigation';

let store = configureStore();
//const Stack = createStackNavigator();

export default function App(props:any) {
	const [isLoadingComplete, setLoadingComplete] = React.useState(false);
	const initval:any = undefined
	const [initialNavigationState, setInitialNavigationState] = React.useState(initval);
	const containerRef = React.useRef();
	const { getInitialState } = useLinking(containerRef);

	React.useEffect(() => {
		async function loadResourcesAndDataAsync() {
			try {
				SplashScreen.preventAutoHide();

				// Load our initial navigation state
				setInitialNavigationState(await getInitialState());
				const auth = await VoFlyAuth.getCachedAuth();
				store.dispatch(setAuthAction(auth));
				// Load fonts
				await Font.loadAsync({
					...Ionicons.font,
					'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
				});
			} catch (e) {
				// We might want to provide this error information to an error reporting service
				console.warn(e);
			} finally {
				setLoadingComplete(true);
				SplashScreen.hide();
			}
		}

		loadResourcesAndDataAsync();
	}, []);

	if (!isLoadingComplete && !props.skipLoadingScreen) {
		return null;
	}
	console.log("OPEN VIEW ====");
	return (
		<Provider store={store}>
			<View style={styles.container}>
				{/*<StatusBar barStyle="default" {...(Platform.OS == "android" ? {backgroundColor: Colors.headerBackgroud} : {})} />
				<NavigationContainer ref={containerRef} initialState={initialNavigationState}>
					<Stack.Navigator>
						{ auth?.isLogged ?
							<Stack.Screen options={
								{header: ()=>null }
							} name="Root" component={AppNavigator} />
							:
							<Stack.Screen options={
								{header: ()=>null }
							} name="Login" component={SignStackNavigator} />
						}
					</Stack.Navigator>
				</NavigationContainer>*/}
				<AuthNavigation containerRef={containerRef} initialState={initialNavigationState} />
			</View>
		</Provider>
	);
}

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "red"// Colors.containerBackground,
	},
	screens: {
		backgroundColor: "transparent"
	}
});
