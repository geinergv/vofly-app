import * as React from 'react';
import {StyleSheet, Platform} from "react-native"
import { createStackNavigator } from '@react-navigation/stack';

import SignInScreen from "../screens/SignInScreen"
import SignUpScreen from "../screens/SignUpScreen"

import Colors from "../constants/Colors"

const SignStack = createStackNavigator();
export default function SignStackNavigator(props:any) {
    return (
        <SignStack.Navigator 
            screenOptions={{
                headerTintColor: Colors.headerTintColor,
                headerStyle:  styles.header,
                //cardStyle: styles.screens,
            }}
        >
            <SignStack.Screen options={{header:()=>null}} name="SignIn" component={SignInScreen} />
            <SignStack.Screen options={{headerTitle: "Registro"}} name="SignUp" component={SignUpScreen} />
        </SignStack.Navigator>
    );
}

const styles = StyleSheet.create({
	header: {
		backgroundColor: Colors.headerBackgroud,
		...Platform.select({
			ios: {
				shadowColor: 'transparent',
				shadowOffset: { width: 0, height: 0 },
				shadowOpacity: 0,
			},
			android: {
				elevation: 0,
			},
			default: {
				borderBottomWidth: 0,
			}
		})
	},
})