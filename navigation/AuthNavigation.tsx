import * as React from 'react';
import { Platform, StatusBar, StyleSheet, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { connect, ConnectedProps } from 'react-redux'
import {RootState} from '../store/redux/reducers';
//import {setAuth, signoutAuth} from '../store/redux/actions';
import { useNavigation } from '@react-navigation/native';
import Colors from "../constants/Colors"


import AppNavigator from './AppStackNavigator';
import SignStackNavigator from "./SignStackNavigator";

const mapState = (state: RootState) => ({
	auth: state.app.auth,
})

const connector = connect(mapState);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
	containerRef: any,
	initialNavigationState: any
};
const Stack = createStackNavigator();

const AuthNavigation:React.FunctionComponent<Props> = ({containerRef, initialNavigationState, ...props}) => {
	console.log("Auth connecto Navigation", props.auth);
	return (
		<>
			<StatusBar barStyle="default" {...(Platform.OS == "android" ? {backgroundColor: Colors.headerBackgroud} : {})} />
			<NavigationContainer ref={containerRef} initialState={initialNavigationState}>
				<Stack.Navigator>
					{ props.auth?.isLogged ?
						<Stack.Screen options={
							{header: ()=>null }
						} name="Root" component={AppNavigator} />
						:
						<Stack.Screen options={
							{header: ()=>null }
						} name="Login" component={SignStackNavigator} />
					}
				</Stack.Navigator>
			</NavigationContainer>
		</>
	);
}

export default connector(AuthNavigation);
