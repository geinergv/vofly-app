import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import TabBarIcon from '../components/TabBarIcon';
import LinksScreen from '../screens/ReservasScreen';
import HomeScreen from '../screens/HomeSreen';
import CuentaScreen from '../screens/CuentaScreen';

//import PerfilScreen from "../screens/cuenta/Perfil";
//import ExampleScreen from '../screens/ExampleScreen';

const BottomTab = createBottomTabNavigator();
//const CuentaStack = createStackNavigator();
const INITIAL_ROUTE_NAME = 'Inicio';

export default function BottomTabNavigator({ navigation, route }:any) {
	// Set the header title on the parent stack navigator depending on the
	// currently active tab. Learn more in the documentation:
	// https://reactnavigation.org/docs/en/screen-options-resolution.html
	navigation.setOptions({ headerTitle: getHeaderTitle(route), headerShown: verHeader(route) });

	return (
		<BottomTab.Navigator initialRouteName={INITIAL_ROUTE_NAME}  
			screenOptions={({route}:any) => ({
				tabBarIcon: ({ focused }:any) => <TabBarIcon focused={focused} id={route.name} />,
				//tabBarVisible: false,
				tabBarLabel: () => null
			})}
		>
			<BottomTab.Screen
				name="Reservas"
				component={LinksScreen}
			/>
			<BottomTab.Screen
				name="Inicio"
				component={HomeScreen}
			/>
			<BottomTab.Screen
				name="Cuenta"
				component={CuentaScreen}
			/>
			{/* <BottomTab.Screen name="fd">
				{({route, navigation})=> {
					
					return null
				}}
			</BottomTab.Screen> */}
		</BottomTab.Navigator>
	);
}

function verHeader(route:any) {
	const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;
	return routeName != "Inicio";
}

function getHeaderTitle(route:any) {
	const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;
	switch (routeName) {
		case 'Reservas':
			return 'Mis pedidos';
		default:
			return routeName;
	}
}