import { useLinking } from '@react-navigation/native';
import { Linking } from 'expo';

export default function(containerRef:any) {
  return useLinking(containerRef, {
    prefixes: [Linking.makeUrl('/')],
    config: {
      Root: {
        path: '',
        screens: {
          Main: {
            initialRouteName: "Inicio",
            screens: {
              Inicio: "inicio",
              Reservas: "reservas",
              Cuenta: "cuenta", /* {
                initialRouteName: "CuentaMain",
                screens: {
                  CuentaMain: "cuenta",
                }
              } */
            }
          },
          Perfil:  "perfil",
          Direcciones:  "direcciones",
          Contacto:  "contacto",
          Condiciones:  "condiciones",
          Pedido: "pedido",
        }
      },
      Login: {
        path: "login",
        screens:  {
          SignIn: "signin",
          SignUp: "signup"
        }
      }
    },
  });
}
