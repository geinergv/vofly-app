import * as React from 'react';
import {StyleSheet, Platform} from "react-native"
import { createStackNavigator } from '@react-navigation/stack';

import BottomTabNagitor from "./BottomTabNavigator"
import PerfilScreen from "../screens/PerfilScreen"
import DireccionesScreen from "../screens/DireccionesScreen"
import ContactoScreen from "../screens/ContactoScreen"
import CondicionesScreen from "../screens/CondicionesScreen"
import PedidoScreen from "../screens/PedidoScreen"

import Colors from "../constants/Colors"

const AppStack = createStackNavigator();
export default function AppStackNavigator(props:any) {
    return (
        <AppStack.Navigator 
            screenOptions={{
                headerTintColor: Colors.headerTintColor,
                headerStyle:  styles.header,
                //cardStyle: styles.screens,
            }}
			//headerTransparent={true}
        >
            <AppStack.Screen name="Main" component={BottomTabNagitor} />
            <AppStack.Screen name="Perfil" component={PerfilScreen} />
            <AppStack.Screen name="Direcciones" component={DireccionesScreen} />
            <AppStack.Screen name="Contacto" component={ContactoScreen} />
            <AppStack.Screen name="Condiciones" component={CondicionesScreen} />
            <AppStack.Screen name="Pedido" component={PedidoScreen} />
        </AppStack.Navigator>
    );
}

const styles = StyleSheet.create({
	header: {
		backgroundColor: Colors.headerBackgroud,
		...Platform.select({
			ios: {
				shadowColor: 'transparent',
				shadowOffset: { width: 0, height: 0 },
				shadowOpacity: 0,
			},
			android: {
				elevation: 0,
			},
			default: {
				borderBottomWidth: 0,
			}
		})
	},
})
