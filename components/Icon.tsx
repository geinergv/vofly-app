import { Ionicons, MaterialCommunityIcons, MaterialIcons, FontAwesome } from "@expo/vector-icons";
import * as React from "react"
import {StyleSheet} from "react-native"

import Colors from "../constants/Colors";

export interface IconProps {
	name?: string,
	size?:number,
	color?: string,
	style?: any,
	from?: string,
	[key: string]: any,
}

export default ({from, ...props}:IconProps) => {
	switch (from) {
		case "materialcommunityicons":
		case "mci":
			return <MaterialCommunityIcons
				{...props}
			/>;
		case "materialicons": 
		case "mi": 
			return <MaterialIcons
				{...props}
			/>;
		case "fa":
			return <FontAwesome
				{...props}
			/>;
		case 'io':
			return <Ionicons {...props} />
		default:
			return <FontAwesome
				{...props}
				name={"user"}
				size={30}
				style={styles.base}
				color={Colors.tintColor}
			/>;
	}
}

const styles = StyleSheet.create({
	base: {/* 
		minHeight: 16,
		justifyContent: "center",
		alignItems: "center",
		display: "flex",
		marginTop:0,
		marginBottom:0,
		paddingTop: 8,
		paddingBottom: 8,
		marginRight: 16,
		padding: 8 */
		//alignSelf: "stretch"
	}
})
