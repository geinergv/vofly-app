import React from "react"
import {TouchableOpacity, TouchableOpacityProps} from "react-native"
import {List} from "react-native-paper"

export interface ListButtonProps {
	buttonProps?: TouchableOpacityProps,
	itemProps: typeof List.Item.defaultProps & {
		title: string
	}
}

export const ListButton:React.FunctionComponent<ListButtonProps> = ({buttonProps, itemProps}) => {
	return (
		<TouchableOpacity {...buttonProps}>
			<List.Item
				{...itemProps}
			/>
		</TouchableOpacity>
	)
}

export default ListButton