import * as React from "react"
import {
	View, StyleSheet, StyleProp, ViewStyle
} from 'react-native';

//import { LinearGradient } from "expo-linear-gradient";

import Colors from "../constants/Colors"
import Layout from "../constants/Layout"

//type StyleType = ReturnType<typeof StyleSheet.create>

export interface DefaultaEstructuraProps {
	title?: string,
	style?: StyleProp<ViewStyle>,
	headerStyle?: StyleProp<ViewStyle>,
	bodyStyle?: StyleProp<ViewStyle>,
	header?(): React.ReactNode,
	body?(): React.ReactNode,
	left?(): React.ReactNode,
	notRounded?: boolean,
}

const DefaultaEstructura:React.FunctionComponent<DefaultaEstructuraProps> = ({header, body, title, notRounded, ...props}) => {
	//if (title) navigation.setOptions({ headerTitle: title });
	const [isLoading, setLoadingState] = React.useState(false);
	const headerEle = header && header()
	const bodyEle = body && body()
	const containerStyle = StyleSheet.compose(styles.container, props.style);
	const headerStyle = StyleSheet.compose(props.headerStyle, styles.header);
	const bodyStyle = StyleSheet.compose({...styles.body, ...(notRounded?{borderTopLeftRadius:0}:{})}, props.bodyStyle);
	return (
		<View style={containerStyle}>
			<View style={headerStyle}>
				{headerEle}
			</View>
			<View style={bodyStyle}>
				{bodyEle}
			</View>
		</View>
	);
}

export default DefaultaEstructura;

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.containerBackground,
	},
	header: {
		//minHeight: 20
	},
	body: {
		backgroundColor: Colors.containerMainBackground,
		borderTopLeftRadius: Layout.RadioSuperiorIzquierdo,
		//overflow: "hidden",
		//flex: 1,
	},
})

export const stylesBothScreen = styles;
