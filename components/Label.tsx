import * as React from "react"
import {StyleSheet, Text, TextProps, StyleProp, TextStyle} from "react-native"
import { Paragraph } from "react-native-paper";

import Layout from "../constants/Layout"

type TextProps2 = React.ComponentPropsWithRef<typeof Text>
type LabelType = "h2" | "p" | "default" | "h4"

type StylesProps = {
	[key in LabelType]: TextStyle
}

export interface LabelProps extends TextProps {
	tipo?: LabelType,
	style?: StyleProp<TextStyle>,
}

const Label:React.FunctionComponent<LabelProps> = ({tipo, style: propStyle, ...props}) => {
	const baseStyle = tipo ? styles[tipo] : styles.default;
	const style = StyleSheet.compose(baseStyle, propStyle);
	switch (tipo) {
		case "p":
			return <Paragraph {...props} style={style}>{props.children}</Paragraph>
	}
	return <Text {...props} style={style}>{props.children}</Text>
}

const styles = StyleSheet.create<StylesProps>({
	default: {
		fontSize: 24,
		fontWeight: "bold",
		marginBottom: 3,
	},
	h2: {
		marginTop: Layout.RadioSuperiorIzquierdo,
		marginBottom: Layout.RadioSuperiorIzquierdo,
		fontWeight: "700",
		fontSize: 22
	},
	p: {
		fontSize: Layout.fontBase,
		marginBottom: 20
	},
	h4: {
		marginTop: 0,
		marginBottom: 20,
		fontWeight: "400",
		fontSize: 18
	}
})

export const stylesLabel = styles
export default Label