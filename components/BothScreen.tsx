import * as React from "react"
import {
	View, StyleSheet, StyleProp, ViewStyle
} from 'react-native';

//import { LinearGradient } from "expo-linear-gradient";

import Colors from "../constants/Colors"
import Layout from "../constants/Layout"

//type StyleType = ReturnType<typeof StyleSheet.create>

export interface BaseScreenProps {
	navigation?: any,
	route?: any,
}

export interface BothScreenProps extends BaseScreenProps {
	title?: string,
	style?: StyleProp<ViewStyle>,
	headerStyle?: StyleProp<ViewStyle>,
	bodyStyle?: StyleProp<ViewStyle>,
	leftStyle?: StyleProp<ViewStyle>,
	header?(): React.ReactNode,
	body?(): React.ReactNode,
	left?(): React.ReactNode,
	notRounded?: boolean,
}

const BothScreen:React.FunctionComponent<BothScreenProps> = ({header, body, left, title, notRounded, ...props}) => {
	//if (title) navigation.setOptions({ headerTitle: title });
	const [isLoading, setLoadingState] = React.useState(false);
	const headerEle = header && header()
	const bodyEle = body && body()
	const leftEle = left && left()
	const all = StyleSheet.compose(styles.container, props.style);
	const leftStyle = StyleSheet.compose(styles.left, props.leftStyle);
	const headerStyle = StyleSheet.compose(props.headerStyle, styles.header);
	const bodyStyle = StyleSheet.compose({...styles.body, ...(notRounded?{borderTopLeftRadius:0}:{})}, props.bodyStyle);
	return (
		<View style={styles.container}>
			{leftEle && <View style={leftStyle}>{leftEle}</View>}
			<View style={{flex: 1, backgroundColor: 'transparent'}}>
				<View style={headerStyle}>
					{headerEle}
				</View>
				<View style={bodyStyle}>
					{bodyEle}
				</View>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.containerBackground,
		flex: 1,
		flexDirection: 'row',
		flexWrap: 'nowrap',
	},
	left: {
		backgroundColor: Colors.containerBackground,
		flexWrap: 'nowrap',
		flexDirection: 'column',
		padding: 10,
		paddingTop: Layout.RadioSuperiorIzquierdo,
	},
	header: {
		minHeight: 10
	},
	body: {
		backgroundColor: Colors.containerMainBackground,
		borderTopLeftRadius: Layout.RadioSuperiorIzquierdo,
		overflow: "hidden",
		flex: 1,
	},
})

export const stylesBothScreen = styles;

export default BothScreen;
