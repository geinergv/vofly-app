import * as React from "react";
import {StyleSheet, TouchableOpacity, View, Image, Text, StyleProp, ViewStyle, TextStyle, Platform} from "react-native"
import Icon from "../components/Icon"
import {Card} from "react-native-paper"
import Layout from "../constants/Layout"
import Colors from "../constants/Colors";

//import "intl"

interface TitleResevaItemProps {
	title: string,
	subtitle: string,
	style?: StyleProp<ViewStyle>,
	titleStyle?: StyleProp<TextStyle>,
}

export const TitleResevaItem:React.FunctionComponent<TitleResevaItemProps> = (props) => {
	const titleStyle = StyleSheet.compose(props.titleStyle, styles.baseTitle)
	return <View style={props.style}>
		<View style={{flexDirection: "column"}}>
			<Text style={titleStyle}>{props.title}</Text>
			<Text style={styles.baseSubtitle}>{props.subtitle}</Text>
		</View>
	</View>
}

interface ReservaItemProps {
	/* cardTitleProps: typeof Card.Title.defaultProps,
	itemBtnProps?: TouchableOpacityProps,
	cardFooterProps?: {
		children: React.ReactNode,
		style: StyleProp<ViewStyle>
	} */
	icon: any,
	tipo: string,
	persona: string,
	direccion: string,
	fecha: string,
	precio: number,
	estado: string,
}

const ReservaItem: React.FunctionComponent<ReservaItemProps> = (props) => {
	return (
		<TouchableOpacity style={styles.item} activeOpacity={0.7}>
			<View style={styles.body}>
				<View style={styles.iconCnt}>
					<Image style={{width: 40, height: 40}} resizeMode="contain" source={props.icon} />
				</View>
				<TitleResevaItem style={{flex: 1}} title={props.tipo} subtitle={props.fecha}/>
				<TitleResevaItem title={props.estado}
					subtitle={
						PrecioFormatString(props.precio)
					}
					titleStyle={props.estado.toLowerCase()=="recogido"? styles.titleResaltado : {}}
				/>
			</View>
			<View style={styles.footer}>
				<Text style={{
					flex: 1,
					fontWeight: "bold",
					lineHeight: 20,
					maxHeight: 20,
				}}>{props.direccion}</Text>
				<View style={{alignItems: "center", flexDirection: "row"}}>
					<Icon from="mi" name="location-on" color={Colors.primary} size={20} />
					<Text>{"    "}</Text>
					<Icon size={17} from="fa" name="send" color={Colors.primary} />
				</View>
				<Text style={{
					flex: 1,
					fontWeight: "bold",
					textAlign: "right",
					lineHeight: 20,
					maxHeight: 20,
					flexWrap: "nowrap"
				}}>{props.persona}</Text>
			</View>
		</TouchableOpacity>
	)
	
	/* (
		<TouchableOpacity {...props.itemBtnProps}>
			<Card>
				<Card.Title {...props.cardTitleProps} title={props.cardTitleProps?.title} />
				{props.cardFooterProps && <Card.Actions {...props.cardFooterProps} children={props.cardFooterProps?.children} />}
			</Card>
		</TouchableOpacity>
	) */
}

function PrecioFormatString(precio:number):string{
	//new Intl.NumberFormat("es-PE", {style: "currency", currency: "PEN"}).format(props.precio)
	return precio.toString()
}

const styles = StyleSheet.create({
	item: {
		backgroundColor: "white",
		marginLeft: 10,
		marginRight: 10,
		overflow: "hidden",
		borderRadius: 10,
		marginBottom: 10,
		...Platform.select({
			ios: {
				shadowColor: Colors.primary,
				shadowOffset: { width: 0, height: -3 },
				shadowOpacity: 0.1,
				shadowRadius: 3,
			},
			android: {
				elevation: 10,
			},
			default: {
				shadowColor: Colors.primary,
				shadowOffset: { width: 0, height: -3 },
				shadowOpacity: 0.2,
				shadowRadius: 3,
			}
		}),
	},
	body: {
		padding: 15,
		flexDirection: "row",
		alignItems: "center"
	},
	iconCnt: {
		marginRight: 10,
		maxWidth: 70
	},
	bottomText: {
		flex: 1,
		fontWeight: "bold",
		overflow: "hidden",
		height: 20,
		lineHeight: 20,
		maxHeight: 20,
	},
	bottomTextRight: {
		textAlign: "right",
	},
	footer: {
		backgroundColor: "#fafafa",
		padding: 15,
		flexDirection: "row"
	},
	baseTitle: {
		fontWeight: "bold",
		fontSize: 18,
		marginBottom: 3,
	},
	titleResaltado: {
		color: Colors.primary,
	},
	baseSubtitle: {
		fontSize: 14,
		color: Colors.textMuted
	},
})

export default ReservaItem
