import * as React from 'react';
import { VoFlyAuth, VerifyPhone } from "../constants/App";
import { Modal, Text, TouchableOpacity, View, Alert, StyleSheet, ScrollView } from 'react-native';


import DefaultaEstructura from './DefaultaEstructura';
import Label from './Label';
import Colors from "../constants/Colors"
import Layout from "../constants/Layout"
import InputItem from "./InputItem";

interface ModalProps {
	modalVisible: boolean,
	close(val:boolean): void,
	verifyData: VerifyPhone,
	submit(code: string): void,
	status?: string
}

const VerificationCodeModal:React.FunctionComponent<ModalProps> = ({modalVisible, close, ...props}) => {
	const [code, setCode] = React.useState("");
	const handleCode = (text:string) => {setCode(text)};
	const handleSubmit = () => {
		if (code.length==6) props.submit(code);
	};
	let explainText = '';
	switch (props.verifyData?.status) {
		case 'NEW_CODE_SENDED':
			explainText = "Se acaba de enviar un nuevo código de confirmación";
			break;
		case 'WAIT_YOUR_CONFIRMATION':
			explainText = "Esperando su confirmación";
			break;
	}
	return <Modal
		animationType="slide"
		transparent={true}
		visible={modalVisible}
		onRequestClose={() => {
			Alert.alert('Si reenvías el mismo teléfono el OTP seguirá siendo válido.');
			close();
		}}>
		<View style={styles.container}>
			<DefaultaEstructura
				style={styles.templateContainer}
				bodyStyle={styles.body}
				body={function(){
					return <View>
						{__DEV__ ? <Text>{JSON.stringify(props.verifyData)}</Text> : null}
						<Text>{explainText}</Text>
						<InputItem value={code} title="Ingrese OTP" keyboardType="numeric" maxLength={6} placeholder="Ingrese los 6 dígitos" onChangeText={handleCode} />
						<TouchableOpacity style={styles.sendBtn} activeOpacity={0.7} onPress={handleSubmit} >
				            <Text style={styles.txtBtn}>Enviar</Text>
				        </TouchableOpacity>
					</View>
				}}
				headerStyle={styles.header}
				header={function(){
					return <Label style={{color: "white", textAlign: "center"}}>Ingrese el código de verificación de 6 dígitos enviado a su número</Label>
				}}
			 />
		</View>
	</Modal>;
}

export default VerificationCodeModal;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "rgba(0,0,0,0.4)",
		justifyContent: "flex-end",
	},
	templateContainer: {
		borderTopLeftRadius: Layout.RadioSuperiorIzquierdo,
		borderTopRightRadius: Layout.RadioSuperiorIzquierdo,
	},
	body: {
		paddingTop: Layout.RadioSuperiorIzquierdo,
		paddingLeft: 15,
		paddingRight: 10,
	},
	header: {
		paddingLeft: 15,
		paddingRight: 10,
		paddingTop: 20,
		paddingBottom: 20,
	},
    sendBtn: {
        marginTop: 10,
		backgroundColor: Colors.primaryLight,
        height: 50,
		justifyContent: "center",
		padding: 10,
		borderTopLeftRadius: Layout.RadioSuperiorIzquierdo,
	},
	txtBtn: {
		fontSize: 22,
		textAlign: "center",
		color: Colors.basePrimary,
	},
});
