import * as React from 'react';
import {StyleSheet, TouchableOpacity, } from 'react-native';
import Colors from '../../constants/Colors';

interface IconDefaultProps {
	size: number,
}

type Props = {
	icon():React.ReactNode,
	selected?: boolean,
	onPress?(): void,
}

const vars = {
	default: {
		size: 50,
		//color: '',
	},
}

const styles = StyleSheet.create({
	button: {
		height: vars.default.size,
		width: vars.default.size,
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: vars.default.size,
		backgroundColor: '#0063a4',
		marginBottom: 10,
	},
	selectedButton: {
		backgroundColor: '#ebeff2',
	},
	icon: {
		
	}
});



const IconTabLeftBar:React.FunctionComponent<Props> = (props) => {
	const icon = {
		default: {
			size: 30,
			color: '#bebebe',
			style: styles.icon,
		},
		select: {
			color: '#006ce3',
		}
	};
	const iconEle = props.icon({...icon.default, ...(props.selected ? icon.select : {})});
	const buttonstyle = props.selected ? StyleSheet.composer(styles.button, styles.selectedButton) : styles.button;
	return <TouchableOpacity style={buttonstyle} onPress={props.onPress}
		activeOpacity={0.6}
	>
		{iconEle}
	</TouchableOpacity>
}

export default IconTabLeftBar;
