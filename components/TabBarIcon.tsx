import { Ionicons, MaterialCommunityIcons, FontAwesome } from "@expo/vector-icons";
import * as React from "react"

import Colors from "../constants/Colors";

interface TabBarIconProps {
	proveedor?: string,
	name?: string,
	focused: boolean,
	id?: string
}

const TabBarIcon:React.FunctionComponent<TabBarIconProps> = (props) => {
	switch ((props.id||props.proveedor||"")) {
		case "Reservas":
		case "reservas-screen":
			return <MaterialCommunityIcons
			name={props.focused ? "bookmark" : "bookmark-outline"}
			size={30}
			style={{ marginBottom: -3 }}
			color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
		/>;
			break;
		case "Inicio":
		case "inicio-screen":
			return <MaterialCommunityIcons
				name={props.focused ? "home" : "home-outline"}
				size={30}
				style={{ marginBottom: -3 }}
				color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
			/>;
			break;
		case "Cuenta":
		case "cuenta-screen":
			return <FontAwesome
				name={props.focused ? "user" : "user-o"}
				size={30}
				style={{ marginBottom: -3 }}
				color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
			/>;
			break;
		case "fa":
		case "font-awesome":
		case "fontawesome":
			return <Ionicons
				name={props.name}
				size={30}
				style={{ marginBottom: -3 }}
				color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
			/>
			break;
		case "MaterialCommunityIcons":
		case "materialcommunityicons":
			return <MaterialCommunityIcons
				name={props.name}
				size={30}
				style={{ marginBottom: -3 }}
				color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
			/>
		default:
			return <Ionicons
				name={props.name}
				size={30}
				style={{ marginBottom: -3 }}
				color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
			/>
			break;
	}
}

export default TabBarIcon