import * as React from "react"
import {View, StyleSheet, StyleProp, ViewStyle, TextProps} from "react-native"
import {TextInput} from "react-native-paper"

import Label from "../components/Label"
import Colors from "../constants/Colors"

type TextInputProps = typeof TextInput.defaultProps

interface Props {
	title: string,
	keyType?: any,
	value?: string,
	editable?: boolean,
    textType?: any,
    style?: StyleProp<ViewStyle>
}

export type InputItemProps = TextInputProps & {
	title: string,
    styleCnt?: StyleProp<ViewStyle>
}

const InputItem:React.FunctionComponent<InputItemProps> = ({title, styleCnt, ...inputProps}) => {
    const style = StyleSheet.compose(styleCnt, styles.group);
	return <View style={style}>
		<Label style={{marginBottom:0, fontWeight: "300"}}>{title}</Label>
		<TextInput 
			style={styles.input}
			{...inputProps}
		/>
	</View>
}

const styles = StyleSheet.create({
    group: {

    },
    input: {
        backgroundColor: Colors.containerMainBackground,
		fontSize: 20,
		
    }
})

export const stylesInputItem = styles

export default InputItem