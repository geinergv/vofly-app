//import React, { useEffect, useState } from 'react';
import { AsyncStorage } from 'react-native';
const URL_SITE = __DEV__ ? "http://192.168.0.6/" : "http://omare6.sg-host.com/"

const AppDta = {
	apiPath: (path:string) => {
		return URL_SITE + "api/" + path;
	}
}
export default AppDta

export interface VerifyPhone {
	verify_id: string,
	reintentos: number,
	updated_at: string,
	[key: string]: any,
}

export interface PreregistrerData {
	name?: string,
	email: string,
	phone: string,
	direccion?: string,
	verify_id?: string,
	verify_id_data?: VerifyPhone,
}

type RegisterSendData = Required<Omit<PreregistrerData, "phone" | "verify_id_data">> & {
	code: string,
}

export type LoginData = Omit<PreregistrerData, "verify_id"> & {
	id: string | number,
	email_verified_at?: string,
	api_token: string,
	avatar?: string,
	created_at: string,
	updated_at: string,
};

export type VoFlyUser = PreregistrerData & Partial<LoginData> & {
	[key: string]: any,
} | null;

let AuthStorageKey = '@VoFly:AppAuthKey';

let configAppApiToken = {
	from: "app",
	token: "a1e25fdb7bfa9de214b453c61df4ba586e3400ec",
}

export interface VoFlyAuth {
	isLogged: boolean,
	isRegistered: boolean
	preregistered: boolean,
	user: VoFlyUser,
	//confirmeLogin(): void
}

type VoFlyAuthType = VoFlyAuth | null

export class VoFlyAuth {
	static auth:VoFlyAuthType = null;
	constructor (user:VoFlyUser = null) {
		this.isLogged = false;
		this.preregistered = false;
		this.isRegistered = false;
		this.user = user;
	}

	setAuth(auth:VoFlyAuthType) {
		if (!auth) {
			this.isLogged = false;
			this.preregistered = false;
			this.user = null;
		} else {
			this.isLogged = auth.isLogged;
			this.preregistered = auth.preregistered;
			this.user = auth.user;
		}
	}

	async saveLocal() {
		console.log("Saved Auth:", this);
		return await VoFlyAuth.setCachedAuth(this);
	}

	static async setCachedAuth(auth:VoFlyAuthType) {
		if (VoFlyAuth.auth) VoFlyAuth.auth.setAuth(auth);
		else VoFlyAuth.auth = auth;
		return await AsyncStorage.setItem(AuthStorageKey, JSON.stringify(VoFlyAuth.auth?.user));
	}

	static async getCachedAuth() {
		if (VoFlyAuth.auth) {
			console.log("Get static auth", VoFlyAuth.auth);
			return VoFlyAuth.auth;
		}
		let value = await AsyncStorage.getItem(AuthStorageKey);
		let authUser:VoFlyUser = value && JSON.parse(value);
		VoFlyAuth.auth = new VoFlyAuth(authUser);
		if (authUser) {
			await VoFlyAuth.auth.confirmLogin();
			await VoFlyAuth.auth.evaluateSignAuthCachedData();
		}
		console.log("Get new auth", VoFlyAuth.auth);
		return VoFlyAuth.auth;
	}

	async confirmLogin() {
		if (this.user && this.user.api_token) {
			const result = await VoFlyAuth.fetcher<{user?:VoFlyUser}>(AppDta.apiPath("user"), {
				headers: {Authorization: `Bearer ${this.user.api_token}`}
			});
			if (result && result.user) {
				this.user = {...this.user, ...result.user};
				this.isLogged = true;
			} else {
				this.isLogged = false;
				this.user.api_token = undefined;
			}
		}
	}

	async evaluateSignAuthCachedData() {
		if (this.user && !this.isLogged && this.user.verify_id) {
			const {phone, email, verify_id} = this.user;
			const result = await VoFlyAuth.fetcher<{data?:VerifyPhone}>(AppDta.apiPath("auth"), {
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					...configAppApiToken,
					phone, email, verify_id
				})
			})
			console.log("confirm preregister result fetcher:", result);
			if (result && result.data) {
				this.preregistered = result.data.preregistered;
				this.isRegistered = !result.data.preregistered;
				this.user.verify_id_data = result.data.verify_data;
				this.user.verify_id = this.user.verify_id_data.verify_id;
			} else {
				this.user.verify_id = undefined;
				this.user.verify_id_data = undefined;
			}
		} else if(this.user) {
			this.user.verify_id = undefined;
			this.user.verify_id_data = undefined;
		}
		await this.saveLocal();
		console.log("end confirm preregister app:", this);
		
	}

	static async preregisterAsync(data:PreregistrerData) {
		const auth = await VoFlyAuth.getCachedAuth(); // ||
		if (auth && !auth.isLogged) {			
			const {verify_id_data, ...pregister_props} = data;
			//const {verify_id} = auth.user;
			const result = await VoFlyAuth.fetcher<{data?:VerifyPhone}>(AppDta.apiPath("preregister"), {
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					...configAppApiToken,
					...pregister_props,
					//...(auth.preregistered ? {verify_id: auth.user?.verify_id} : {})
				})
			});
			if (result && result.data) {
				auth.user = data
				auth.user.verify_id = result.data.verify_id;
				auth.user.verify_id_data = result.data;
				auth.preregistered = true;
				await auth.saveLocal();
			} else {
				if (result?.errors?.verify_id) {
					auth.user.verify_id = undefined;
					auth.user.verify_id_data = undefined;
					auth.preregistered = false;
					await auth.saveLocal();
				}
			}
			return result;
		}
		return {message: "Logeado"};
	}

	static async registerAsync(data:RegisterSendData) {
		const auth = await VoFlyAuth.getCachedAuth(); // ||
		if (auth && !auth.isLogged && auth.preregistered) {
			//const {direccion, name, verify_id, code} = data;
			const result = await VoFlyAuth.fetcher<{data?:LoginData}>(AppDta.apiPath("register"), {
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					...configAppApiToken,
					...data
				})
			});
			if (result && result.data) {
				auth.user = result.data;
				auth.isLogged = true;
				await auth.saveLocal();
			} else {	
				if (result?.errors?.verify_id) {
					auth.user.verify_id = undefined;
					auth.user.verify_id_data = undefined;
					await auth.saveLocal();
				}
			}
			return result;
		}
		return null;
	}

	static async preloginAsync(email:string, phone: string, verify_id?:string="") {
		const auth = await VoFlyAuth.getCachedAuth(); // ||
		if (auth && !auth.isLogged) {
			//const {verify_id_data, ...pregister_props} = data;
			//const {verify_id} = auth.user;
			const predata= {email, phone,
					...(!auth.preregistered && verify_id ? {verify_id} : {})};
			console.log(predata);
			const result = await VoFlyAuth.fetcher<{data?:VerifyPhone}>(AppDta.apiPath("prelogin"), {
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					...configAppApiToken,
					email, phone,
					...(!auth.preregistered && verify_id ? {verify_id} : {})
				})
			});
			if (result && result.data) {
				auth.user = {email, phone};
				auth.user.verify_id = result.data.verify_id;
				auth.user.verify_id_data = result.data;
				auth.isRegistered = true;
				await auth.saveLocal();
			} else {
				if (result?.errors?.verify_id) {
					auth.user.verify_id = undefined;
					auth.user.verify_id_data = undefined;
					await auth.saveLocal();
				}
				auth.isRegistered = false
			}
			return result;
		}
		return {message: "Logeado"};
	}

	static async loginAsync(verify_id:string, code: string) {
		const auth = await VoFlyAuth.getCachedAuth(); // ||
		if (auth && !auth.isLogged && auth.isRegistered) {
			//const {verify_id_data, ...pregister_props} = data;
			//const {verify_id} = auth.user;
			const result = await VoFlyAuth.fetcher<{data?:LoginData}>(AppDta.apiPath("login"), {
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					...configAppApiToken,
					verify_id, code
				})
			});
			console.log("result login async", result);
			if (result && result.data) {
				auth.user = result.data;
				auth.isLogged = true;
				await auth.saveLocal();
			} else {
				if (result?.errors?.verify_id) {
					auth.user.verify_id = undefined;
					auth.user.verify_id_data = undefined;
					await auth.saveLocal();
				}
			}
			return result;
		}
		return {message: "Logeado"};
	}

	static async signoutAsync() {
		const auth = await VoFlyAuth.getCachedAuth();
		if (auth && auth.isLogged) {
			auth.user = null;
			auth.isLogged = false;
			auth.preregistered = false;
			await auth.saveLocal();
		}
		return auth
	}

	static async fetcher<T={}>(url:string, options:RequestInit) {
		const {headers, ...rest} = options;
		const init:RequestInit = {
			method: "POST", headers: {
				'Content-Type': 'multipart/form-data',
				"Accept": "application/json",
				...headers
			},
			...rest
		};
		return await fetch(url, init).then(res=>{
			return res.json()
		}).then<T & {
			message?: string, errors?:{[key:string]: any},
			exception?: string,
		}>(val => {
			return val;
		})
	}
}

export async function preregisterAsync() {

}

export async function signInAsync() {

}
  
export async function cacheAuthAsync(authState:object) {
	return await AsyncStorage.setItem(AuthStorageKey, JSON.stringify(authState));
  }
  
  export async function getCachedAuthAsync() {
	let value = await AsyncStorage.getItem(AuthStorageKey);
	let authState = value && JSON.parse(value);
	console.log('getCachedAuthAsync', authState);
	/* if (authState) {
	  if (checkIfTokenExpired(authState)) {
		return refreshAuthAsync(authState);
	  } else {
		return authState;
	  }
	}
	return null;*/
	return authState;
  }
  
  function checkIfTokenExpired({ accessTokenExpirationDate }:any) {
	return new Date(accessTokenExpirationDate) < new Date();
  }
  
  async function refreshAuthAsync({ refreshToken }:any) {
	/* let authState = await AppAuth.refreshAsync(config, refreshToken);
	console.log('refreshAuth', authState);
	await cacheAuthAsync(authState);
	return authState; */
  }
  
  export async function signOutAsync({ accessToken }:any) {
	/* try {
	  await AppAuth.revokeAsync(config, {
		token: accessToken,
		isClientIdProvided: true,
	  });
	  await AsyncStorage.removeItem(StorageKey);
	  return null;
	} catch (e) {
	  alert(`Failed to revoke token: ${e.message}`);
	} */
  }
