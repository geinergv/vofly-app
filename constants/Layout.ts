import {Dimensions, StyleSheet} from "react-native"

const wid = Dimensions.get("window").width
const hei = Dimensions.get("window").height

export interface BaseScreenProps {
	navigation?: any,
	route?: any,
}

export default {
	window: {
		wid,
		hei,
	},
	isSmallDevice: wid < 375,

	RadioSuperiorIzquierdo: 30,

	fontBase: 16
}