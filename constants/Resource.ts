
interface Resource {
    imagen(path:string): any
}

class ResourceFactory implements Resource {
    imagen(path: string) {
        const val = require(path)
        return val;
    }
}

const Resource = new ResourceFactory()

export default Resource