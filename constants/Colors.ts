const primaryB = "#0d70d5";
const primary = "#0071e4";
const primaryLight = "#00c3c9";
const basePrimary = "#ffffff";
export default {
	primary,
	basePrimary,
	primaryLight,
	
	containerBackground: primary,
	containerMainBackground: basePrimary,
	containerHomeBackground: "#cce0f9",

	tintColor: primary,
	tintColorOnPrimary: basePrimary,

	tabIconDefault: '#cccccc',
	tabIconSelected: primary,
	tabBar: '#fefefe',
	errorBackground: 'red',
	errorText: '#ffffff',
	warningBackground: '#EAEB5E',
	warningText: '#666804',
	noticeBackground: primary,
	noticeText: '#ffffff',

	headerBackgroud: primary,
	headerTintColor: basePrimary,

	textMuted: "#6c757d",
	link: "#007bff",
}